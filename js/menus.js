var elemento = localStorage.getItem('key');
const pestaña = document.getElementsByTagName('title')
pestaña.textContent = elemento;
const menu = JSON.parse(localStorage.getItem(elemento));
const nav = document.querySelector('.lista');
crearMenu(menu);
function crearMenu(elemento) {
    for (let index = 0; index < elemento.length; index++) {
        var a = document.createElement("a");
        var li = document.createElement("li");
        if (elemento[index].sub === '') {
            a.setAttribute("href", elemento[index].link);
            a.setAttribute("target", "complemento")
            a.textContent = elemento[index].name;
            li.appendChild(a)
            li.setAttribute("class","item");
            nav.appendChild(li);
        } else {
            li.textContent = elemento[index].name;
            li.setAttribute("class","subtemaPadre");
            var ul = document.createElement("ul");
            ul.setAttribute("class","sublist")
            var subtema = elemento[index].sub;
            li.appendChild(ul)
            for (let i = 0; i < subtema.length; i++) {
                var sli = document.createElement("li");
                var as = document.createElement("a")
                as.setAttribute("href", subtema[i].link);
                as.setAttribute("target", "complemento");
                as.textContent = subtema[i].name;
                sli.appendChild(as);
                sli.setAttribute("class","item");
                ul.appendChild(sli); 
            }
            nav.appendChild(li);  
        }
    }
};
const submenu = document.querySelectorAll('.subtemaPadre');
const sublist = document.querySelectorAll('.sublist')
morstrarSubmenu(submenu);
function morstrarSubmenu(record) {
    for (let i = 0; i < record.length; i++) {
        record[i].onclick = function () {
            this.classList.toggle('active');
            sublist[i].classList.toggle('ver')
        }
    } 
}