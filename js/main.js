cargarLocal();
function cargarLocal(){
    const html = [
        {name:'pagina base',link:'html/HTMLBASICS.html',sub:'' },
        {name:'historia HTML',sub:''},
        {name:'Etiquetas',sub:''},
        {name: 'HTML semantico', sub: [
            {name: 'Accesibilidad',sub:''}]
        },
        {name:'S.E.O search engine optimiser',sub:''},
        {name:'curiosidades',sub:''}
    ];
    const css = [
        {name:'Css Basico',link:'css/css_page.html',sub:'' },
        {name:'Selectores',link:'css/selectorsCss.html',sub:'' },
        {name:'Mapa HTML-CSS',link:'css/ConceptosBasicoFrontend.html',sub:'' },
        {name:'Presentacion - Jessica carrillo',link:'css/adicional/clase-semillero.pdf',sub:'' },
        {name:'Introduccion',sub:''},
        {name:'Selectores',sub:[
            {name:'Selectores basicos',sub:''},
            {name:'Pseudo selectores',sub:''},
            {name:'Selectores anidados',sub:''}
        ]},
        {name:'Aprendiendo CSS básico',sub:[
            {name:'Dimensinamiento whidth - heigth',sub:''},
            {name:'Fuentes,tamaño y color de letra',sub:''},
            {name:'Fondos (background)',sub:''},
            {name:'Esaciados Margin - Padding',sub:''},
            {name:'Bordes',sub:''},
        ]},
        {name:'Pocicionamiento',sub:[
            {name:'Float',sub:''},
            {name:'Posición absolute - relative',sub:''},
            {name:'Posición por coordenada',sub:''},
        ]},
        {name:'Layouts',sub:[
            {name:'CSS grid',sub:''},
            {name:'Flex-box',sub:''},
        ]},
        {name:'Media querys',sub:''}
    ];
    const js =[
        {name:'Historia JavaScript',link:'JS/historiaJS.html',sub:''},
        {name:'SINTAXIS Y CONSTRUCTORES',link:'JS/SintaxisJS.html',sub:''},
        {name:'Funciones Array',link:'JS/arrays.html',sub:''},
        {name:'JAVASCRIPT 30',link:'JS/JS30.HTML',sub:''},
        {name:'Historia JavaScript',sub:''},
        {name:'Tipos de datos primitivos',sub:''},
        {name:'Ámbito de las variables',sub:[
            {name:'Let',sub:''},
            {name:'Const',sub:''},
            {name:'Var',sub:''}
        ]},
        {name:'Sintaxis',sub:[
            {name:'Declaracion de funciones',sub:''},
            {name:'Arrow functions',sub:''},
            {name:'Funcionmes anonimas',sub:''},
            {name:'Sentencias Basicas',sub:''},
        ]},
        {name:'Metodos de Array',sub:[
            {name:'map',sub:''},
            {name:'reduce',sub:''},
            {name:'concat',sub:''},
            {name:'every',sub:''},
            {name:'fill',sub:''},
            {name:'filter',sub:''},
            {name:'find',sub:''},
            {name:'findIndex',sub:''},
            {name:'join',sub:''},
            {name:'reduceRight',sub:''},
            {name:'split',sub:''},
            {name:'shift',sub:''},
            {name:'pop',sub:''},
            {name:'unshift',sub:''},
            {name:'push',sub:''},
            {name:'reverse',sub:''},
            {name:'some',sub:''},
            {name:'sort',sub:''}
        ]},
        {name:'DOM y su manipulacion',sub:[
            {name:'selectores JS ?',sub:''},
            {name:'Interpolacion',sub:''},
            {name:'HTML desde JavaScript',sub:''},
        ]},
        {name:'Eventos',sub:''}
    ];
    const conocimientos = [
        {name:'Comprension Lectura', link:'pages_conocimientos/comprencionLectura.html',sub:''},
        {name:'internet y HTTP REQUEST', link:'pages_conocimientos/internet.html',sub:''}
    ]

    localStorage.setItem('HTML',JSON.stringify(html));
    localStorage.setItem('CSS',JSON.stringify(css));
    localStorage.setItem('JS',JSON.stringify(js));
    localStorage.setItem('CONOCIMIENTOS',JSON.stringify(conocimientos));
}
var tarjetas = document.querySelectorAll('.tarjeta')
tarjetas.forEach(element => element.addEventListener('click', selecionarMenu));
var opcion;
function selecionarMenu() {
     opcion = this.classList[1];
    console.log(opcion);
    switch (opcion) {
        case 'HTML':
            localStorage.setItem('key',opcion);      
            break;
        case 'CSS':
            localStorage.setItem('key',opcion);
            break;
        case 'JS':
            localStorage.setItem('key',opcion);
            break;
        case 'CONOCIMIENTOS':
            localStorage.setItem('key',opcion);
            break;
        default:
            break;
    }
    window.location.assign('pages/Pprincipal.html');
}

const colapse = document.querySelectorAll('.colapse');
const desplegar = document.querySelectorAll('.ltbuttom');
mostrar(colapse);
mostrar(desplegar);
function mostrar(record) {
    for (let i = 0; i < record.length; i++) {
        record[i].onclick = function () {
            console.log(record[i]);
            this.classList.toggle('active');
            this.nextElementSibling.classList.toggle('ver');
        }
    }
}