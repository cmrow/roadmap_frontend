var jsonObj = [
    {
        idTema: 1,
        subtema: "map",
        title: "Método map (Array)",
        summary: "Map crea un nuevo arreglo con el resultado de la llamada a la función indicada aplicado a cada un de los elementos",
        example: "url"

    },
    {
        idTema: 1,
        subtema: "map",
        title: "Método map (Array)",
        summary: "Map crea un nuevo arreglo con el resultado de la llamada a la función indicada aplicado a cada un de los elementos",
        example: "url"
    }
];

const title = document.querySelector('#title');
const titlePrincipal = document.querySelector('#titlePrincipal');
const summary = document.querySelector('#pSummary');
const iframe = document.querySelector('#iframe');

function firtLoad() {
    title.textContent = jsonObj.title;
    titlePrincipal.textContent = jsonObj.subtema;
    summary.innerHTML = `<b>Resumen : </b>${jsonObj.summary}`;
    iframe.setAttribute('src', example)
}

window.addEventListener('load', firtLoad);